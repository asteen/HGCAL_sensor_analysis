stages:
    - compile
    - unittest
    - format
    - documentation

#######################
# Compilation targets #
#######################

cmp:slc6-gcc:
    stage: compile
    tags:
        - docker
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:slc6-llvm:
    stage: compile
    tags:
        - docker
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:centos7-gcc:
    stage: compile
    tags:
        - docker
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:centos7-llvm:
    stage: compile
    tags:
        - docker
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:mac1013-clang:
    stage: compile
    tags:
        - mac
    script:
        - source .gitlab-ci.d/init_mac.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

##############
# Unit tests #
##############

ut:slc6-gcc:
    stage: unittest
    tags:
        - docker
    dependencies:
        - cmp:slc6-gcc
    image: clicdp/slc6-base
    script:
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ctest --output-on-failure -j4 -V

ut:slc6-llvm:
    stage: unittest
    tags:
        - docker
    dependencies:
        - cmp:slc6-llvm
    image: clicdp/slc6-base
    script:
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ctest --output-on-failure -j4 -V

ut:centos7-gcc:
    stage: unittest
    tags:
        - docker
    dependencies:
        - cmp:centos7-gcc
    image: clicdp/cc7-base
    script:
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ctest --output-on-failure -j4 -V

ut:centos7-llvm:
    stage: unittest
    tags:
        - docker
    dependencies:
        - cmp:centos7-llvm
    image: clicdp/cc7-base
    script:
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ctest --output-on-failure -j4 -V

# ut:mac1013-clang:
#     stage: unittest
#     tags:
#         - mac
#     dependencies:
#     - cmp:mac1013-clang
#     script:
#         - source .gitlab-ci.d/init_mac.sh
#         - cd build/
#         - ctest --output-on-failure -j4 -V
#     allow_failure: true

############################
# Format Checking #
############################

fmt:slc6-llvm-format:
    stage: format
    tags:
        - docker
    dependencies:
        - cmp:slc6-llvm
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - make check-format

fmt:centos7-llvm-format:
    stage: format
    tags:
        - docker
    dependencies:
        - cmp:centos7-llvm
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - make check-format

############################
# Lint Checking #
############################

lnt:slc6-llvm-lint:
    stage: format
    tags:
        - docker
    dependencies:
        - cmp:slc6-llvm
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - make check-lint

lnt:centos7-llvm-lint:
    stage: format
    tags:
        - docker
    dependencies:
        - cmp:centos7-llvm
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - make check-lint

#############################
# Documentation Compilation #
#############################

doc:doxygen:
    stage: documentation
    tags:
        - docker
    image: clicdp/cc7-base
    dependencies: []
    script:
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir -p public/
        - mkdir build
        - cd build
        - cmake -DBUILD_DOCS_ONLY=ON ..
        - make doc
        - mv ../doc/html ../public/
    artifacts:
        paths:
            - public
        expire_in: 3 hour
