#!/usr/bin/python

from __future__ import print_function

import math
import numpy


print('Creating regular geofile...')

filename = 'regular_geo.txt'

patterns = ['HexEdges', 'Hex', 'Hex30', 'Switch', 'FCAL', 'Hex8in198ch']
pattern = patterns[5]

MIN_X = -6.0
MAX_X = 6.0
MIN_Y = -6.0
MAX_Y = 5.5

padtype = 0
current_padnum = 1



print('Generating regular geo file with pattern', pattern)


def print_header(myfile):
    print('#', file=myfile)
    print('# This is an automatically generated geo file', file=myfile)
    print('# Script: create_regular_geofile.py', file=myfile)
    print('#', file=myfile)
    print('# Draws all available cell shapes', file=myfile)
    print('#', file=myfile)
    print('# padnumber	xposition	yposition	type	optional\n\n', file=myfile)

if pattern == 'HexEdges':
    padtype = 0
    padsize = 0.80
    padrot = 0

    chan_dist_x = -1.7
    chan_dist_y = -1.9

    current_x = padsize / 2
    current_y = MAX_Y + padsize / 2

    def draw_diamond(x, y, rot):
        global current_padnum
        current_padnum += 1
        return str(current_padnum-1) + '\t' + '{0:+.2f}'.format(x) + '\t' + str(y) + '\t' + str(23) + '\tSIZE:' + str(padsize*0.8) + ',ROTATE:' + str(rot)

    def draw_hex_row(x_center, y_center, npads, edge):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize*2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            this_y = y_center
            this_padtype = padtype
            this_padrot = padrot
            if edge == 1:
                this_padtype = 22
                this_padrot = 90
            elif edge == 2:
                this_padtype = 22
                this_padrot = 270
            elif edge == 3:
                if i_chan == 0:
                    this_padtype = 22
                    this_padrot = 150
                elif i_chan == npads-1:
                    this_padtype = 22
                    this_padrot = 30
            elif edge == 4:
                if i_chan == 0:
                    this_padtype = 22
                    this_padrot = 210
                elif i_chan == npads-1:
                    this_padtype = 22
                    this_padrot = 330
            yield str(current_padnum) + '\t' + '{0:+.2f}'.format(this_x) + '\t' + '{0:+.2f}'.format(this_y) + '\t' + str(this_padtype) + '\tSIZE:' + str(padsize) + ',ROTATE:' + str(this_padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize*2
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            this_edge = 0
            if i_row == 0:
                this_edge = 1
                this_npads = 8
            elif i_row < 9:
                this_edge = 3
                this_npads = i_row+10
            elif i_row == 9:
                this_npads = i_row+8
            elif i_row == nrows-1:
                this_edge = 2
                this_npads = 8
            else:
                this_edge = 4
                this_npads = 9 + nrows - i_row
            yield draw_hex_row(x_center, current_y, this_npads, this_edge)
            current_y -= paddist_y

    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 19)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)
        lefthexoffset = 3.5
        ritehexoffset = 3.4
        print(draw_diamond(MIN_X+lefthexoffset*padsize, MAX_Y+0.2*padsize, 300), file=myfile)
        print(draw_diamond(MAX_X-ritehexoffset*padsize, MAX_Y+0.2*padsize, 240), file=myfile)
        print(draw_diamond(MIN_X-0.6*padsize, 0.07*padsize, 0), file=myfile)
        print(draw_diamond(MAX_X+0.65*padsize, 0.07*padsize, 180), file=myfile)
        print(draw_diamond(MIN_X+lefthexoffset*padsize, MIN_Y+0.55*padsize, 60), file=myfile)
        print(draw_diamond(MAX_X-ritehexoffset*padsize, MIN_Y+0.55*padsize, 120), file=myfile)

if pattern == 'Hex':
    padtype = 0
    padsize = 0.80
    padrot = 0

    chan_dist_x = -1.7
    chan_dist_y = -1.9

    current_x = padsize / 2
    current_y = MAX_Y - padsize / 2

    def draw_hex_row(x_center, y_center, npads):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize*2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            this_y = y_center
            yield str(current_padnum) + '\t' + '{0:+.2f}'.format(this_x) + '\t' + '{0:+.2f}'.format(this_y) + '\t' + str(padtype) + '\tSIZE:' + str(padsize) + ',ROTATE:' + str(padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize*2
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            if i_row < 9:
                this_npads = i_row+9
            else:
                this_npads = 8 + nrows - i_row
            yield draw_hex_row(x_center, current_y, this_npads)
            current_y -= paddist_y

    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 17)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)

elif pattern == 'Hex30':
    padtype = 0
    padsize = 0.8
    padrot = 30

    chan_dist_x = -2.2
    chan_dist_y = -0.85

    current_x = padsize
    current_y = MAX_Y + padsize * 2 / 3

    def draw_hex_row(x_center, y_center, npads):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize*6/2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            this_y = y_center
            yield str(current_padnum) + '\t' + '{0:+.2f}'.format(this_x) + '\t' + '{0:+.2f}'.format(this_y) + '\t' + str(padtype) + '\tSIZE:' + str(padsize) + ',ROTATE:' + str(padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            if i_row < 9:
                this_npads = i_row+1
            elif i_row < 25:
                this_npads = 9 - i_row % 2
            elif i_row < 33:
                this_npads = nrows - i_row
            yield draw_hex_row(x_center, current_y, this_npads)
            current_y -= paddist_y

    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 33)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)


elif pattern == 'Switch':
    padtype = 12
    current_padnum = 0

    padsize = 0.4

    chan_dist_x = 0.2
    chan_dist_y = 0.3

    current_x = MIN_X
    current_y = MAX_Y

    def draw_multiplexer(x_center, y_center):
        global current_padnum
        paddist_x = chan_dist_x/2 + padsize/2
        paddist_y = chan_dist_y/2 + padsize/2
        for i_chan in range(0, 8):
            this_x = 0
            if i_chan % 2 == 0:
                this_x = x_center - paddist_x
            else:
                this_x = x_center + paddist_x
            this_y = y_center + 2 * paddist_y - int(i_chan / 2) * paddist_y
            yield str(current_padnum) + '\t' + '{0:+.2f}'.format(this_x) + '\t' + '{0:+.2f}'.format(this_y) + '\t' + str(padtype) + '\tSIZE:' + str(padsize)
            current_padnum += 1

    def draw_multiplexer_col(x_center):
        for i_col in range(0, 8):
            this_y = MAX_Y - i_col * (MAX_Y - MIN_Y) / 7
            yield draw_multiplexer(x_center, this_y)

    def draw_multiplexer_rows():
        for i_row in range(0, 8):
            this_x = MIN_X + i_row * (MAX_X - MIN_X) / 7
            yield draw_multiplexer_col(this_x)

    with open(filename, 'w') as myfile:
        print_header(myfile)
        rows = draw_multiplexer_rows()
        for row in rows:
            for col in row:
                for plex in col:
                    print(plex, file=myfile)


elif pattern == 'FCAL':
    padtype = 12
    current_padnum = 1

    padsize = 0.45

    chan_dist_x = 0.3
    chan_dist_y = -0.035

    current_x = MIN_X + (MAX_X - MIN_X) * 0.09
    current_y = MIN_Y + (MAX_Y - MIN_Y) * 0.54

    nCols = 4
    nRows = 64

    def draw_FCAL_col(x_center, angle):
        global current_padnum
        y_center = current_y + (MAX_Y - MIN_Y) / 2 + padsize - 0.06 * abs(angle)
        paddist_y = chan_dist_y/2 + padsize/2 - 0.0005 * abs(angle)
        for i_row in range(0, nRows):
            xStretch = 10 - 6 / nRows * i_row
            thisCos = math.cos(angle/360 * 2 * math.pi)
            maxShift = -0.1 + abs(0.5 * angle)
            this_x = x_center + 0.5 * maxShift * i_row / nRows * numpy.sign(angle) * thisCos
            this_y = y_center + 2 * paddist_y - i_row * paddist_y
            yield str(current_padnum) + '\t' + '{0:+.2f}'.format(this_x) + '\t' + '{0:+.2f}'.format(this_y) + '\t' + str(padtype) + '\tSIZE:' + str(padsize) + ',X_STRETCH:' + '{0:.2f}'.format(xStretch) + ',Y_STRETCH:0.45,BEND:0.05,ROTATE:' + '{0:.2f}'.format(angle)
            current_padnum += 1

    def draw_FCAL_cols():
        maxAngle = 12
        for i_col in range(0, nCols):
            angle = maxAngle - 2 * maxAngle / (nCols - 1) * i_col
            this_x = current_x + i_col * (MAX_X - MIN_X) / nCols + i_col * chan_dist_x
            yield draw_FCAL_col(this_x, angle)

    with open(filename, 'w') as myfile:
        print_header(myfile)
        cols = draw_FCAL_cols()
        for col in cols:
            for plex in col:
                print(plex, file=myfile)
        # print('257	+0.00	-0.10	29	SIZE:19.9,ROTATE:45,Y_STRETCH:0.5,WIDTH:0.01,BEND:0.06', file=myfile)

elif pattern == 'Hex8in198ch':
    padtype = 0
    padsize = 0.82
    padrot = 0

    chan_dist_x = -1.7
    chan_dist_y = -1.9

    current_x = padsize / 2
    current_y = MAX_Y - padsize / 2

    def draw_hex_row(x_center, y_center, row, npads, edge):
        global current_padnum
        this_padsize = padsize
        paddist_x = chan_dist_x/2 + padsize*2
        for i_chan in range(0, npads):
            this_x = x_center + i_chan * paddist_x - (npads * paddist_x) / 2
            if row == 1 or row == 5 or row == 6 or row == 12:
               this_x = x_center + i_chan * paddist_x - ((npads-1) * paddist_x) / 2 
            if row == 11:
               this_x = x_center + i_chan * paddist_x - ((npads-2) * paddist_x) / 2 
            this_y = y_center
            this_padtype = padtype
            this_padrot = padrot
            if edge == 1:
                if i_chan == 0:
                    this_padtype = 11
                    this_padrot = "-120,X_CROP:0.5,INVERT:1"
                elif i_chan == npads-1:
                    this_padtype = 11
                    this_padrot = "-120,X_CROP:0.5,ROTATE:120"
                else:                
                    this_padtype = 11
                    this_padrot = "0"
            elif edge == 2:
                if i_chan == 0: #edge left
                    this_padtype = 41
                    this_padrot = 60
		    #set cell 86
                    if row == 7:
	                    this_padtype = 42
	                    this_padrot = "-60,INVERT:1"
                    if row == 1:
	                    this_padtype = 42
	                    this_padrot = "60"
                elif i_chan == npads-1: #edge right
                    if row == 1 or row == 5 or row == 6:
                        this_x = x_center + (i_chan-1) * paddist_x - ((npads-1) * paddist_x) / 2
                    this_padtype = 41
                    this_padrot = 300
		    #set cell 111
                    if row == 7:
	                    this_padtype = 42
	                    this_padrot = 300
                    if row == 1:
	                    this_padtype = 42
	                    this_padrot = "60,INVERT:1"
                #set calibration 13
                elif i_chan == 4 and row == 1: #cell 13
                    this_padtype = 4
                    this_padrot = 0
                elif i_chan > 4 and row == 1: #from calibration cell 14
                    this_padsize = 0.82
                    this_x = x_center + (i_chan-1) * paddist_x - ((npads-1) * paddist_x) / 2
                    if i_chan == 5:
                        this_padsize = 0.35
                        this_padtype = 14
                #set calibration 62
                elif i_chan == 9 and row == 5: #cell 61
                    this_padtype = 4
                    this_padrot = 0
                elif i_chan > 9 and row == 5: #from calibration cell 62
                    this_padsize = 0.82
                    this_x = x_center + (i_chan-1) * paddist_x - ((npads-1) * paddist_x) / 2
                    if i_chan == 10:
                        this_padsize = 0.35
                        this_padtype = 14
                #set calibration 70
                elif i_chan == 3 and row == 6: #cell 69
                    this_padtype = 4
                    this_padrot = 0
                elif i_chan > 3 and row == 6: #from calibration cell 70
                    this_padsize = 0.82
                    this_x = x_center + (i_chan-1) * paddist_x - ((npads-1) * paddist_x) / 2
                    if i_chan == 4:
                        this_padsize = 0.35
                        this_padtype = 14
            elif edge == 3:
                if i_chan == 0:
                    this_padtype = 11
                    this_padrot = "-120,X_CROP:0.5,ROTATE:-120"
                elif i_chan == npads-1:
                    this_padtype = 11
                    this_padrot = "-120,X_CROP:0.5,ROTATE:60"
            elif edge == 4:
                if i_chan == 0:
                    this_padtype = 11
                    this_padrot = 120
                elif i_chan == npads-1:
                    this_padsize = 0.82
                    this_padtype = 11
                    this_padrot = -120
                    if row == 11:
                        this_x = x_center + (i_chan-2) * paddist_x - ((npads-2) * paddist_x) / 2
                    if row == 12:
                        this_x = x_center + (i_chan-1) * paddist_x - ((npads-1) * paddist_x) / 2
                #set calibration 143
                elif i_chan == 1 and row == 11: #cell 142
                    this_padtype = 4
                    this_padrot = 0
                elif i_chan > 1 and i_chan < 12 and row == 11: #from calibration cell 144
                    this_padsize = 0.82
                    this_x = x_center + (i_chan) * paddist_x - ((npads) * paddist_x) / 2
                    if i_chan == 2:
                        this_padsize = 0.35
                        this_padtype = 14
                #set calibration 154
                elif i_chan == 12 and row == 11: #cell 153
                    this_padtype = 4
                    this_padrot = 0
                    this_x = x_center + (i_chan) * paddist_x - ((npads) * paddist_x) / 2
                elif i_chan == 13 and row == 11: #calibration cell 154
                    this_padsize = 0.35
                    this_padtype = 14
                    this_x = x_center + (i_chan-2) * paddist_x - ((npads-2) * paddist_x) / 2
                #set calibration 163
                elif i_chan == 6 and row == 12: #cell 162
                    this_padtype = 4
                    this_padrot = 0
                elif i_chan > 6 and row == 12: #from calibration cell 163
                    this_padsize = 0.82
                    this_x = x_center + (i_chan-1) * paddist_x - ((npads-1) * paddist_x) / 2
                    if i_chan == 7:
                        this_padsize = 0.35
                        this_padtype = 14
            elif edge == 5:
                if i_chan == 0:
                    this_padtype = 11
                    this_padrot = "-120,X_CROP:0.5,ROTATE:-60"
                elif i_chan == 1:
                    this_padtype = 42
                    this_padrot = 180
                elif i_chan == npads-2:
                    this_padtype = 42
                    this_padrot = "-180,INVERT:1"
                elif i_chan == npads-1:
                    this_padtype = 11
                    this_padrot = "-120,X_CROP:0.5"
                else:                
                    this_padtype = 41
                    this_padrot = 180
            yield str(current_padnum) + '\t' + '{0:+.2f}'.format(this_x) + '\t' + '{0:+.2f}'.format(this_y) + '\t' + str(this_padtype) + '\tSIZE:' + str(this_padsize) + ',ROTATE:' + str(this_padrot)
            current_padnum += 1

    def draw_hex_rows(x_center, nrows):
        paddist_y = chan_dist_y/2 + padsize*2
        for i_row in range(0, nrows):
            global current_y
            this_npads = 0
            this_edge = 0
            if i_row == 0: #first row
                this_edge = 1
                this_npads = 8
            elif i_row < 8: #upper part
                 this_edge = 2
                 this_npads = i_row+8
                 if i_row == 1 or i_row == 5 or i_row == 6:
                    this_npads = i_row+9
            elif i_row == 8: #centre row
                this_edge = 3
                this_npads = i_row+8
            elif i_row == nrows-1: #last row
                this_edge = 5
                this_npads = 9
            else: #lower part
                this_edge = 4
                this_npads = 8 + nrows - i_row
                if i_row == 11:
                    this_npads = 10 + nrows - i_row
                if i_row == 12:
                    this_npads = 9 + nrows - i_row
            yield draw_hex_row(x_center, current_y, i_row, this_npads, this_edge)
            current_y -= paddist_y


    with open(filename, 'w') as myfile:
        print_header(myfile)

        rows = draw_hex_rows(current_x, 16)
        for row in rows:
            for cell in row:
                print(cell, file=myfile)
