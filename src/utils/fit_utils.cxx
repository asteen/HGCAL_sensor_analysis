#include "fit_utils.h"

// use fancy contructor magic to populate the class member variables
ranged_fit_func::ranged_fit_func(Double_t (*f)(Double_t*, Double_t*), std::vector<double> r)
    : ranges(std::move(std::move(r))), fitfunc(f){};

// when an instance of this class is used to construct a TF1, the operator method is used as the function
Double_t ranged_fit_func::operator()(Double_t* x, Double_t* par) {
  // should we use this point in the fit?
  bool accept_point = false;

  // if no ranges are given, default to the whole range
  if (ranges.empty()) {
    accept_point = true;
  }

  // test if the point is within one of the ranges
  for (unsigned int i = 0; i < ranges.size(); i += 2) {
    if (i < (ranges.size() - 1)) {
      accept_point |= ((ranges.at(i) < x[0]) && (x[0] < ranges.at(i + 1)));  // NOLINT
    }

    // deal with the case where an odd number of numbers are given
    if (i == (ranges.size() - 1)) {
      accept_point |= (ranges.at(i) < x[0]);  // NOLINT
    }
  }

  // if it's in none of the ranges, reject it from the fit
  if (!accept_point) {
    TF1::RejectPoint();
  }

  // return the value either way, otherwise it doesn't plot correctly
  return fitfunc(x, par);
};
