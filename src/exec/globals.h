/**
        @file globals.h
        @brief A collection of global variables
  @author Andreas Alexander Maier
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <TGraph.h>

const int MAX_CELL_NUMBER = 1000;
const int MAX_CELL_TYPE = 100;
const int MAX_SENSOR_NUMBER = 200;
const int MAX_DETAILED_PLOT_NUMBER = 1000;
const int MAX_SELECTOR_NUMBER = 500;
const double AUTOPAD_SELECTOR = -20000000;
const double SKIP_VALUE = -1000000;
const double CANV_WIDTH = 750;
const double MAX_ALLOWED_PAD_CONTENT = 1E+20;

extern int N_VALID_FILES;
extern std::string OUTPUT_DIR;
extern std::string VALID_SENSOR_NAMES[MAX_SENSOR_NUMBER];
extern TGraph* GR_CONTENTS[MAX_CELL_NUMBER];
extern double SENSOR_VALS_FOR_AVERAGE[MAX_SENSOR_NUMBER][MAX_CELL_NUMBER];

#endif
