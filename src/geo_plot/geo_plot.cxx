#include "geo_plot.h"

double HEX_TRAFO = 2. / sqrt(3.);

geo_plot::geo_plot() {
  h_vals = new TH2F("h_vals", "h_vals", 100, -7, 7, 100, -7, 7);
  h_colors = new TH1F("h_colors", "h_colors", MAX_PAD_NUMBER, 0, MAX_PAD_NUMBER);
  for (size_t i = 0; i < MAX_PAD_NUMBER; i++) {
    h_colors->SetBinContent(i + 1, DEFAULT_COLOR);
  }
  for (int i = 0; i < MAX_INTERCELL_NUMBER; ++i) {
    h_vals_inter[i] = new TH2F(Form("h_vals_inter[%d]", i), Form("h_vals_inter[%d]", i), 100, -7, 7, 100, -7, 7);
    h_colors_inter[i] =
        new TH1F(Form("h_colors_inter[%d]", i), Form("h_colors_inter[%d]", i), MAX_PAD_NUMBER, 0, MAX_PAD_NUMBER);
    for (size_t j = 0; j < MAX_PAD_NUMBER; j++) {
      h_colors_inter[i]->SetBinContent(j + 1, DEFAULT_COLOR);
    }
  }
  geoTree = nullptr;
  nPads = 0;
  nNegativePadNums = 0;
  for (int i = 0; i < MAX_PAD_NUMBER; ++i) {
    padTypeScale[i] = 1;
    padScale[i] = 1;
  }
  verbose = 0;
  testInfo = false;
  noAxis = false;
  overwritePadColors = -1;
  zeroBased = false;
  treatOtherPads = "dotted";
  infoOption = "ALL";
  geoFileIsRead = false;
  outputFile = "hex.png";
  zTitle = "";
  topLeftInfo = "";
  lowRightInfo = "";
  lowLeftInfo = "";
  topRightInfo = "";
  textColor = kBlack;
  geoFile = "";
  padNumOpt = 0;
  zMin = 0;
  zMax = 0;
  valPrecision = 1;
  drawInterPadValues = false;
  nInterCells = 0;
  paletteNumber = 1;
}
void geo_plot::reset() {
  h_vals->Reset();
  h_colors->Reset();
  geoTree = nullptr;
  nPads = 0;
  nNegativePadNums = 0;
  for (int i = 0; i < MAX_PAD_NUMBER; ++i) {
    padTypeScale[i] = 1;
    padScale[i] = 1;
    h_colors->SetBinContent(i + 1, DEFAULT_COLOR);
  }
  for (auto& i : h_colors_inter) {
    for (size_t j = 0; j < MAX_PAD_NUMBER; j++) {
      i->SetBinContent(j + 1, DEFAULT_COLOR);
    }
  }
  outputFile = "hex.png";
  zMin = 0;
  zMax = 0;
  highlightPads.clear();
}
int geo_plot::get_pad_type(int padNum) {
  fill_geo_info();
  for (unsigned int i = 0; i < padNumbers.size(); ++i) {
    if (padNum == padNumbers.at(i)) {
      return padTypes.at(i);
    }
  }
  return -1;
}
std::vector<int> geo_plot::get_pad_type_vec() {
  fill_geo_info();
  std::vector<int> padTypeVec;
  for (size_t i = 0; i < padNumbers.size(); i++) {
    padTypeVec.push_back(get_pad_type(i + 1));
  }
  return padTypeVec;
}
std::vector<std::string> geo_plot::get_pad_type_string_vec() {
  fill_geo_info();
  std::vector<std::string> padTypeStringVec;
  for (size_t i = 0; i < padNumbers.size(); i++) {
    padTypeStringVec.push_back(get_pad_type_string(i + 1));
  }
  return padTypeStringVec;
}
double geo_plot::get_pad_surface(int padType) {
  // areas in square micron
  double standardarea = 108824232.749822;
  double padarea = 1;
  if (padType == 0) {
    padarea = 108824232.749822;  // 1.;// "standard";
  } else if (padType == 1) {
    padarea = 54601907.9709;  // 1.4;// "guard ring";
  } else if (padType == 2) {
    padarea = 53815632.292657;  // 0.5;// "half cell";
  } else if (padType == 3) {
    padarea = 37258667.86556;  // 0.4;// "mouse bite";
  } else if (padType == 4) {
    padarea = 93016364.267491;  // 0.7;// "around calibration cell";
  } else if (padType == 5) {
    padarea = 14527691.4154;  // 0.3;// "calibration cell";
    //                   padarea=109214686.563422;//d=20um
    //                   padarea=108824232.749822;//d=40um
    //                   padarea=108437842.789322;//d=60um
    //                   padarea=108052139.999822;//d=80um
  } else if (padType == 6) {
    padarea = standardarea * 0.7;  // "test capacity"
  } else if (padType == 10) {
    padarea = 58258917.8096164;  // jumper on HPK 8inch
  } else {
    return 1.;
  }
  return padarea / standardarea;
}
std::vector<double> geo_plot::get_pad_surface_vec() {
  std::vector<double> padSurfaceVec;
  const int MY_MAX_CELL_TYPE = 100;
  for (size_t i = 0; i < MY_MAX_CELL_TYPE; i++) {
    padSurfaceVec.push_back(get_pad_surface(i));
  }
  return padSurfaceVec;
}
std::string geo_plot::get_type_string(int padType) {
  // clang-format off
  if (padType == 0) {  return "standard"; }
  if (padType == 1) {  return "guard ring"; }
  if (padType == 2) {  return "half cell"; }
  if (padType == 3) {  return "mouse bite"; }
  if (padType == 4) {  return "hex ring"; }
  if (padType == 5) {  return "calibration cell"; }
  if (padType == 6) {  return "test capacity"; }
  if (padType == 7) {  return "pentagram"; }
  if (padType == 8) {  return "hexagram"; }
  if (padType == 9) {  return "heptagram"; }
  if (padType == 10) { return "jumper cell"; }
  if (padType == 11) { return "cornercut cell"; }
  if (padType == 12) { return "square cell"; }
  if (padType == 13) { return "triangle cell"; }
  if (padType == 14) { return "circle cell"; }
  if (padType == 15) { return "plus"; }
  if (padType == 16) { return "heart"; }
  if (padType == 17) { return "pentagon cell"; }
  if (padType == 18) { return "heptagon cell"; }
  if (padType == 19) { return "octagon cell"; }
  if (padType == 20) { return "tetragram"; }
  if (padType == 21) { return "edge half cell"; }
  if (padType == 22) { return "edge small half cell"; }
  if (padType == 23) { return "diamond cell"; }
  if (padType == 24) { return "octagram"; }
  if (padType == 25) { return "arrow"; }
  if (padType == 26) { return "flash"; }
  if (padType == 27) { return "person"; }
  if (padType == 28) { return "triangle ring"; }
  if (padType == 29) { return "square ring"; }
  if (padType == 30) { return "pent ring"; }
  if (padType == 31) { return "hept ring"; }
  if (padType == 32) { return "oct ring"; }
  if (padType == 33) { return "ring"; }
  if (padType == 34) { return "flower"; }
  if (padType == 35) { return "bar trigram"; }
  if (padType == 36) { return "bar pentagram"; }
  if (padType == 37) { return "bar hexagram"; }
  if (padType == 38) { return "bar heptagram"; }
  if (padType == 39) { return "bar octagram"; }
  if (padType == 40) { return "moon"; }
  return "unspecified";
  // clang-format on
}
std::string geo_plot::get_pad_type_string(int padNum) {
  int padType = get_pad_type(padNum);
  return get_type_string(padType);
}
std::string geo_plot::extract_additional_parameters(const std::string& addPars) {
  std::string resultString;
  std::vector<std::string> parameterList = split(addPars, ',');
  for (auto& i : parameterList) {
    std::vector<std::string> parValue = split(i, ':');
    std::string thisParName = parValue.at(0);
    if (thisParName == "TEXT") {
      i = replace_string_all(i, "~", " ");
    }
    if (!resultString.empty()) {
      resultString += ",";
    }
    resultString += i;
  }
  return resultString;
}
std::string geo_plot::get_par_val(const std::string& padTrafo, const std::string& parString) {
  std::vector<std::string> parameterList = split(padTrafo, ',');
  for (const auto& i : parameterList) {
    std::string thisParVal;
    std::string thisParName;
    std::vector<std::string> parValue = split(i, ':');
    if (parValue.size() > 1) {
      thisParName = parValue.at(0);
      thisParVal = parValue.at(1);
      if (thisParName == parString) {
        return thisParVal;
      }
    } else {
      thisParName = i;
      if (thisParName == parString) {
        return "1";
      }
    }
  }
  return "";
}
double geo_plot::get_par_val_double(const std::string& padTrafo, const std::string& parString) {
  std::string resultString = get_par_val(padTrafo, parString);
  return std::atof(resultString.c_str());
}
std::string geo_plot::replace_par_val(const std::string& padTrafo, const std::string& parString,
                                      const std::string& newVal) {
  std::vector<std::string> parameterList = split(padTrafo, ',');
  std::string resultString;
  bool found = false;
  for (const auto& i : parameterList) {
    std::vector<std::string> parValue = split(i, ':');
    std::string thisParName = parValue.at(0);
    if (!resultString.empty()) {
      resultString += ",";
    }
    if (thisParName == parString) {
      resultString += parString;
      resultString += ":";
      resultString += newVal;
      found = true;
    } else {
      resultString += i;
    }
  }
  if (!found) {
    if (!resultString.empty()) {
      resultString += ",";
    }
    resultString += parString + ":" + newVal;
  }
  return resultString;
}
double geo_plot::get_total_sum_par_val_double(const std::string& padTrafo, const std::string& parString) {
  std::vector<std::string> parameterList = split(padTrafo, ',');
  double valSum = 0;
  for (const auto& i : parameterList) {
    valSum += get_par_val_double(i, parString);
  }
  return valSum;
}
void geo_plot::fill_geo_info() {
  if (!std::ifstream(geoFile)) {
    ERROR_MSG("Warning: file %s doesn't exist!", geoFile.c_str());
    exit(0);
  }
  if (geoFileIsRead) {
    return;
  }
  std::string formatString = "num/I:xPos/D:yPos/D:type/I:addParsTree/C";
  geoTree = new TTree("geoTree", "geoTree");
  geoTree->ReadFile(geoFile.c_str(), formatString.c_str());
  int num, type;
  double xPos, yPos;
  const int CHAR_LENGTH = 256;
  char addParsTree[CHAR_LENGTH] = "";
  geoTree->SetBranchAddress("num", &num);
  geoTree->SetBranchAddress("xPos", &xPos);
  geoTree->SetBranchAddress("yPos", &yPos);
  geoTree->SetBranchAddress("type", &type);
  geoTree->SetBranchAddress("addParsTree", addParsTree);
  padNumbers.clear();
  padXPositions.clear();
  padYPositions.clear();
  padTypes.clear();
  padTrafos.clear();

  for (int i = 0; i < geoTree->GetEntries(); ++i) {
    strncpy(addParsTree, "", sizeof(addParsTree));
    geoTree->GetEntry(i);
    std::string addPars(addParsTree, addParsTree + CHAR_LENGTH);
    std::string padTrafo = extract_additional_parameters(addPars);

    if (num == 0) {
      zeroBased = true;
    }
    DEBUG_MSG("Filling geo data: num %d type %d x %.2f y %.2f trafo %s", num, type, xPos, yPos, padTrafo.c_str());
    if (is_in_vector(num, padNumbers)) {
      WARNING_MSG("padNumber %d appears more than once in geometry file! Overwriting last value!", num);
      int pos = find(padNumbers.begin(), padNumbers.end(), num) - padNumbers.begin();
      padNumbers.at(pos) = num;
      padXPositions.at(pos) = xPos;
      padYPositions.at(pos) = yPos;
      padTypes.at(pos) = type;
      padTrafos.at(pos) = padTrafo;
    } else {
      padNumbers.push_back(num);
      padXPositions.push_back(xPos);
      padYPositions.push_back(yPos);
      padTypes.push_back(type);
      padTrafos.push_back(padTrafo);
      if (num < 0) {
        ++nNegativePadNums;
      }
    }
  }
  geoFileIsRead = true;
}
int geo_plot::get_bin_number(int bin_number_no_overflow) {
  int bin_number(0);
  for (int i = 0; i < h_vals->GetSize(); ++i) {
    if (h_vals->IsBinOverflow(i)) {
      continue;
    }
    if (h_vals->IsBinUnderflow(i)) {
      continue;
    }
    ++bin_number;
    if (bin_number == bin_number_no_overflow) {
      return i;
    }
  }
  return -1;
}
void geo_plot::adjust_z_range() {
  double plotmax = -1e20;
  double plotmin = 1e20;
  for (int pad = 0; pad <= nPads; ++pad) {
    if (!is_in_vector(pad, padNumbers)) {
      continue;
    }
    int bin = get_bin_number(pad);
    int type = get_pad_type(pad);
    if (type == -1) {
      DEBUG_MSG("Setting content of pad %d from %.2e to 0!", pad, h_vals->GetBinContent(bin));
      h_vals->SetBinContent(bin, 0);
    }
    double bincont = h_vals->GetBinContent(bin);
    bincont *= padTypeScale[type];
    bincont *= padScale[pad];
    if (zeroSuppress && bincont == 0) {
      DEBUG_MSG("Skipping  bin %d pad %d with cont %f because it is empty", bin, pad, bincont);
      continue;
    }
    if (bincont > MAX_DOUBLE) {
      DEBUG_MSG("Skipping  bin %d pad %d because it's content is larger than maximum double: %e > %e", bin, pad,
                bincont, MAX_DOUBLE);
      continue;
    }
    // INFO_MSG("Processing bin %d pad %d with cont %f and type %d with padScale
    // %f",bin,pad,bincont,type,padScale[pad]);
    if (plotmax < bincont) {
      plotmax = bincont;
    }
    if (plotmin > bincont) {
      plotmin = bincont;
    }
  }
  // INFO_MSG("Final plotmax %f",plotmax );
  h_vals->SetMaximum(plotmax);
  h_vals->SetMinimum(plotmin);
}
void geo_plot::fill(int bin, double value) {
  h_vals->SetBinContent(get_bin_number(bin), value);
  DEBUG_MSG("Setting hex bin content %d (%d global) to %.3e", bin, get_bin_number(bin),
            h_vals->GetBinContent(get_bin_number(bin)));
  ++nPads;
}
void geo_plot::fill(TH1F* h_fill) {
  for (int bin = 0; bin < h_fill->GetNbinsX(); ++bin) {
    h_vals->SetBinContent(get_bin_number(bin + 1), h_fill->GetBinContent(bin + 1));
    DEBUG_MSG("Setting hex bin content %d (%d global) to %.3e (should be %.3e)", bin + 1, get_bin_number(bin + 1),
              h_vals->GetBinContent(get_bin_number(bin + 1)), h_fill->GetBinContent(bin + 1));
    ++nPads;
  }
  // INFO_MSG("loaded values of %d pads!",nPads);
}
void geo_plot::fill_inter_cell(TH1F* h_fill, int interIdx) {
  if (interIdx >= MAX_INTERCELL_NUMBER) {
    WARNING_MSG("Warning! Maximum number of inter-cell displays reached! (%d >= %d) Skipping this!", interIdx,
                MAX_INTERCELL_NUMBER);
    return;
  }
  for (int bin = 0; bin < h_fill->GetNbinsX(); ++bin) {
    h_vals_inter[interIdx]->SetBinContent(get_bin_number(bin + 1), h_fill->GetBinContent(bin + 1));
    DEBUG_MSG("Setting inter-cell %d of bin %d (%d global) to %.3e (should be %.3e)", interIdx, bin + 1,
              get_bin_number(bin + 1), h_vals_inter[interIdx]->GetBinContent(get_bin_number(bin + 1)),
              h_fill->GetBinContent(bin + 1));
    ++nPads;
  }
  ++nInterCells;
  drawInterPadValues = true;
}
void geo_plot::draw_white_box() {
  TPaveText* whitebox;
  double x_min = h_vals->GetXaxis()->GetBinLowEdge(h_vals->GetXaxis()->GetFirst());
  double x_max = h_vals->GetXaxis()->GetBinUpEdge(h_vals->GetXaxis()->GetLast());
  double y_min = h_vals->GetYaxis()->GetBinLowEdge(h_vals->GetYaxis()->GetFirst());
  double y_max = h_vals->GetYaxis()->GetBinUpEdge(h_vals->GetYaxis()->GetLast());
  whitebox = new TPaveText(x_min, y_min - (y_max - y_min) * 0.1, x_max + (x_max - x_min) * 0.01, y_max * 1.01);
  whitebox->SetFillColor(kWhite);
  whitebox->SetLineColor(kNone);
  whitebox->SetFillStyle(1001);
  whitebox->Draw("same");
}
void geo_plot::draw_intercell(double x, double y, const std::string& trafo, int col, int style) {
  int colorArray[2 + MAX_INTERCELL_NUMBER] = {col, col};
  draw_circle(x, y, trafo, colorArray, style);
}
void geo_plot::draw_intercells(TPolyLine* pline, int npar, double side, int colorArray[2 + MAX_INTERCELL_NUMBER],
                               int interCellStyle, int starthere) {
  if (interCellStyle == 0) {
    return;
  }
  double* rot_xvals = pline->GetX();
  double* rot_yvals = pline->GetY();
  int maxintercell = TMath::Min(nInterCells, npar - 1);
  for (int i = 0; i < maxintercell; ++i) {
    int j = positive_modulo(i + 1, npar - starthere) + starthere;
    int k = positive_modulo(i, npar - starthere) + starthere;
    double inter_x = (rot_xvals[k] + rot_xvals[j]) / 2;
    double inter_y = (rot_yvals[k] + rot_yvals[j]) / 2;
    double howfarin = 0.05;
    // parallel to edge
    double inter_xlength = rot_xvals[k] - rot_xvals[j];
    double inter_ylength = rot_yvals[k] - rot_yvals[j];
    double inter_rot = 180 + TMath::ATan(inter_ylength / inter_xlength) / (2 * TMath::Pi()) * 360;
    // decide which side of edge and move ellipse a bit inside
    double xoffset = fabs(TMath::Cos((90 - inter_rot) / 360 * 2 * TMath::Pi()) * side * howfarin);
    double yoffset = fabs(TMath::Sin((90 - inter_rot) / 360 * 2 * TMath::Pi()) * side * howfarin);
    double* center = get_poly_center_of_gravity(pline);
    inter_x = inter_x + ((inter_x < center[0]) ? 1 : -1) * xoffset;
    inter_y = inter_y + ((inter_y < center[1]) ? 1 : -1) * yoffset;
    // determine length according to edge length
    double length = 0.8 * sqrt(pow(inter_xlength, 2) + pow(inter_ylength, 2));
    std::string interTrafo = "SIZE:" + double_to_string(length) +
                             ",Y_STRETCH:" + double_to_string(0.125 * side / length) +
                             ",ROTATE:" + double_to_string(inter_rot);
    if (colorArray[2 + i] != 0) {
      draw_intercell(inter_x, inter_y, interTrafo, colorArray[2 + i], interCellStyle);
    }
  }
}
TPolyLine* geo_plot::create_poly(int n, double x, double y, double xvals[], double yvals[], const std::string& trafo,
                                 int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  auto* pline = new TPolyLine(n, xvals, yvals);
  pline->SetFillColor(colorArray[0]);
  pline->SetFillStyle(style);
  pline->SetLineColor(colorArray[1]);
  std::vector<std::string> parameterList = split(trafo, ',');
  for (const auto& i : parameterList) {
    std::string thisParName;
    std::string thisParVal;
    std::vector<std::string> parValue = split(i, ':');
    if (parValue.size() > 1) {
      thisParName = parValue.at(0);
      thisParVal = parValue.at(1);
    } else {
      thisParName = i;
    }
    // clang-format off
    if (thisParName == "ROTATE") { rotate_polygon(pline, x, y, std::atof(thisParVal.c_str()));
    } else if (thisParName == "X_CROP") { crop_polygon(pline, std::atof(thisParVal.c_str()), 0);
    } else if (thisParName == "Y_CROP") { crop_polygon(pline, 0, std::atof(thisParVal.c_str()));
    } else if (thisParName == "X_STRETCH") { stretch_polygon(pline, std::atof(thisParVal.c_str()), 1);
    } else if (thisParName == "Y_STRETCH") { stretch_polygon(pline, 1, std::atof(thisParVal.c_str()));
    } else if (thisParName == "POST_ROTATE") { rotate_polygon(pline, x, y, std::atof(thisParVal.c_str()));
    } else if (thisParName == "X_SHEAR") { shear_polygon(pline, std::atof(thisParVal.c_str()), 0);
    } else if (thisParName == "Y_SHEAR") { shear_polygon(pline, 0, std::atof(thisParVal.c_str()));
    } else if (thisParName == "INVERT") { invert_polygon(pline, x, y);
    } else if (thisParName == "BEND") { bend_polygon(pline, std::atof(thisParVal.c_str()));
    } else if (thisParName == "CUT") { continue;
    } else if (thisParName == "SIZE") { continue;
    } else if (thisParName == "TEXT") { continue;
    } else if (thisParName == "WIDTH") { continue;
    } else if (thisParName == "TEXT_X_OFFSET") { continue;
    } else if (thisParName == "TEXT_Y_OFFSET") { continue;
    } else if (thisParName == "EDGE_SHIFT") { continue;
    } else {
      WARNING_MSG("Keyword '%s' in geo file is not known!", thisParName.c_str());
    }
    // clang-format on
  }
  return pline;
}
TPolyLine* geo_plot::draw_polygram(double x, double y, const std::string& trafo, int corners,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  int skipCorners = int(get_par_val_double(trafo, "CUT"));
  if (skipCorners > corners - 3) {
    WARNING_MSG("Cutting %d spikes from %d spike polygram makes no sense. Abort cutting!", skipCorners, corners);
    skipCorners = 0;
  }
  double side = get_par_val_double(trafo, "SIZE");
  double width = get_par_val_double(trafo, "WIDTH");
  if (width == 0.0) {
    width = 0.5;
  }
  int npar = 2 * corners;
  double xvals[MAX_CELL_CORNER_NUMBER];
  double yvals[MAX_CELL_CORNER_NUMBER];
  for (int i = 0; i < npar + 1 - 2 * skipCorners + 1; ++i) {
    double angle = i * 2 * TMath::Pi() / npar;
    double thisSide = (i % 2 == 1) ? side : side * width;
    xvals[i] = x + thisSide / 2 * TMath::Sin(angle);
    yvals[i] = y + thisSide / 2 * TMath::Cos(angle);
  }
  xvals[npar - 2 * skipCorners + 1] = xvals[0];
  yvals[npar - 2 * skipCorners + 1] = yvals[0];
  TPolyLine* pline = create_poly(npar - 2 * skipCorners + 2, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_tetragram(double x, double y, const std::string& trafo,
                                    int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_polygram(x, y, trafo, 4, colorArray, style);
}
TPolyLine* geo_plot::draw_pentagram(double x, double y, const std::string& trafo,
                                    int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_polygram(x, y, trafo, 5, colorArray, style);
}
TPolyLine* geo_plot::draw_hexagram(double x, double y, const std::string& trafo,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_polygram(x, y, trafo, 6, colorArray, style);
}
TPolyLine* geo_plot::draw_heptagram(double x, double y, const std::string& trafo,
                                    int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_polygram(x, y, trafo, 7, colorArray, style);
}
TPolyLine* geo_plot::draw_octagram(double x, double y, const std::string& trafo,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_polygram(x, y, trafo, 8, colorArray, style);
}
TPolyLine* geo_plot::draw_equipoly(double x, double y, const std::string& trafo, int corners,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  int skipCorners = int(get_par_val_double(trafo, "CUT"));
  if (skipCorners > corners - 3) {
    WARNING_MSG("Cutting %d corners from %d corner polygon makes no sense. Abort cutting!", skipCorners, corners);
    skipCorners = 0;
  }
  double side = get_par_val_double(trafo, "SIZE");
  int npar = corners;
  double xvals[MAX_CELL_CORNER_NUMBER];
  double yvals[MAX_CELL_CORNER_NUMBER];
  for (int i = 0; i < npar + 1 - skipCorners; ++i) {
    double angle = i * 2 * TMath::Pi() / corners;
    xvals[i] = x + side / 2 * TMath::Sin(angle);
    yvals[i] = y + side / 2 * TMath::Cos(angle);
  }
  xvals[npar - skipCorners] = xvals[0];
  yvals[npar - skipCorners] = yvals[0];
  TPolyLine* pline = create_poly(npar + 1 - skipCorners, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, corners - skipCorners, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_triangle(double x, double y, const std::string& trafo,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly(x, y, trafo, 3, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_square(double x, double y, std::string trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                 int style, int interCellStyle) {
  trafo.insert(0, "ROTATE:45,");
  return draw_equipoly(x, y, trafo, 4, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_pent(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                               int style, int interCellStyle) {
  return draw_equipoly(x, y, trafo, 5, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_hex(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                              int style, int interCellStyle) {
  return draw_equipoly(x, y, trafo, 6, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_hept(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                               int style, int interCellStyle) {
  return draw_equipoly(x, y, trafo, 7, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_oct(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                              int style, int interCellStyle) {
  return draw_equipoly(x, y, trafo, 8, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_circle(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                 int style) {
  return draw_equipoly(x, y, trafo, 100, colorArray, style, 0);
}
TPolyLine* geo_plot::draw_hex_half(double x, double y, std::string trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                   int style, int interCellStyle) {
  trafo.append(",ROTATE:180");
  trafo = replace_par_val(trafo, "CUT", "2");
  return draw_equipoly(x, y, trafo, 6, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_hex_cornercut(double x, double y, std::string trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                        int style, int interCellStyle) {
  trafo.append(",ROTATE:300");
  trafo = replace_par_val(trafo, "CUT", "1");
  return draw_equipoly(x, y, trafo, 6, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_equipoly_ring(double x, double y, const std::string& trafo, int corners,
                                        int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  int skipCorners = int(get_par_val_double(trafo, "CUT"));
  if (skipCorners > corners - 3) {
    WARNING_MSG("Cutting %d corners from %d corner polygon ring makes no sense. Abort cutting!", skipCorners, corners);
    skipCorners = 0;
  }
  double side = get_par_val_double(trafo, "SIZE");
  double width = get_par_val_double(trafo, "WIDTH");
  if (width == 0.0) {
    width = 0.4;
  }
  int npar = 2 * corners + 2;
  double xvals[MAX_CELL_CORNER_NUMBER];
  double yvals[MAX_CELL_CORNER_NUMBER];
  int idx = 0;
  double tunedShiftFactor = skipCorners * side * (1. - width) * 1. / log(corners - 1.) / 7.;
  double xTrafo =
      x + tunedShiftFactor * TMath::Sin(TMath::Pi() / 2 + ((corners - 2.) / 2. - skipCorners) * TMath::Pi() / corners);
  double yTrafo =
      y + tunedShiftFactor * TMath::Cos(TMath::Pi() / 2 + ((corners - 2.) / 2. - skipCorners) * TMath::Pi() / corners);
  while (idx < npar / 2 - skipCorners - 1) {
    double angle = idx * 2 * TMath::Pi() / corners;
    // INFO_MSG("%d. inner angle %f", idx, angle / (2 * TMath::Pi()) * 360 );
    xvals[idx] = xTrafo + side / 2 * (1 - width) * TMath::Sin(angle);
    yvals[idx] = yTrafo + side / 2 * (1 - width) * TMath::Cos(angle);
    ++idx;
  }
  int idxTransition = idx;
  xvals[idxTransition] = xvals[0];
  yvals[idxTransition] = yvals[0];
  ++idx;
  while (idx < npar - 2 * skipCorners - 2 + 1) {
    double angle = (idx - 1 - idxTransition) * 2 * TMath::Pi() / corners;
    // INFO_MSG("%d. outer angle %f", idx, angle / (2 * TMath::Pi()) * 360 );
    xvals[idx] = x + side / 2 * TMath::Sin(angle);
    yvals[idx] = y + side / 2 * TMath::Cos(angle);
    ++idx;
  }
  xvals[idx] = xvals[idxTransition + 1];
  yvals[idx] = yvals[idxTransition + 1];
  ++idx;
  TPolyLine* pline = create_poly(idx, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");

  int starthere = corners + 1;
  if (drawInterPadValues) {
    draw_intercells(pline, npar - 2 * skipCorners, side, colorArray, interCellStyle, starthere);
  }
  return pline;
}
TPolyLine* geo_plot::draw_triangle_ring(double x, double y, const std::string& trafo,
                                        int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly_ring(x, y, trafo, 3, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_square_ring(double x, double y, const std::string& trafo,
                                      int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly_ring(x, y, trafo, 4, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_pent_ring(double x, double y, const std::string& trafo,
                                    int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly_ring(x, y, trafo, 5, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_hex_ring(double x, double y, const std::string& trafo,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly_ring(x, y, trafo, 6, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_hept_ring(double x, double y, const std::string& trafo,
                                    int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly_ring(x, y, trafo, 7, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_oct_ring(double x, double y, const std::string& trafo,
                                   int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  return draw_equipoly_ring(x, y, trafo, 8, colorArray, style, interCellStyle);
}
TPolyLine* geo_plot::draw_circle_ring(double x, double y, const std::string& trafo,
                                      int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_equipoly_ring(x, y, trafo, 100, colorArray, style, 0);
}
TPolyLine* geo_plot::draw_hex_mousebite(double x, double y, const std::string& trafo,
                                        int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  const int npar = 6;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x;
  xvals[1] = x + side / HEX_TRAFO;
  xvals[2] = x;
  xvals[3] = x - side / HEX_TRAFO;
  xvals[4] = x - side / HEX_TRAFO;
  xvals[5] = xvals[0];
  yvals[0] = y;
  yvals[1] = y - side / 2;
  yvals[2] = y - side;
  yvals[3] = y - side / 2;
  yvals[4] = y;
  yvals[5] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_hex_cornercut_long(double x, double y, const std::string& trafo,
                                             int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  const int npar = 6;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x - side / HEX_TRAFO;
  xvals[1] = x + side / HEX_TRAFO;
  xvals[2] = x + side / HEX_TRAFO;
  xvals[3] = x;
  xvals[4] = x - side / HEX_TRAFO;
  xvals[5] = xvals[0];
  yvals[0] = y + side / HEX_TRAFO;
  yvals[1] = y + side / HEX_TRAFO;
  yvals[2] = y - side / 2;
  yvals[3] = y - side;
  yvals[4] = y - side / 2;
  yvals[5] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_hex_cornercut_long_cut(double x, double y, const std::string& trafo,
                                                 int colorArray[2 + MAX_INTERCELL_NUMBER], int style,
                                                 int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  const int npar = 6;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x - side / HEX_TRAFO;
  xvals[1] = x + side / HEX_TRAFO / 3;
  xvals[2] = x + side / HEX_TRAFO;
  xvals[3] = x + side / HEX_TRAFO;
  xvals[4] = x;
  xvals[5] = x - side / HEX_TRAFO;
  yvals[0] = y + side / HEX_TRAFO;
  yvals[1] = y + side / HEX_TRAFO;
  yvals[2] = y + side * HEX_TRAFO / 2;
  yvals[3] = y - side / 2;
  yvals[4] = y - side;
  yvals[5] = y - side / 2;
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_hex_otherhalf(double x, double y, const std::string& trafo,
                                        int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  double smallerfrac = 0;  // remove even more of the cell
  const int npar = 6;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x;
  xvals[1] = x;
  xvals[2] = x - (1 - smallerfrac) * side / 2;
  xvals[3] = x - side;
  xvals[4] = x - (1 - smallerfrac) * side / 2;
  xvals[5] = xvals[0];
  yvals[0] = y + side / HEX_TRAFO;
  yvals[1] = y - side / HEX_TRAFO;
  yvals[2] = y - side / HEX_TRAFO;
  yvals[3] = y;
  yvals[4] = y + side / HEX_TRAFO;
  yvals[5] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_diamond(double x, double y, const std::string& trafo,
                                  int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  double smallerfrac = 0.7;  // remove even more of the cell
  const int npar = 6;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x + (1 - smallerfrac) * side / HEX_TRAFO;
  xvals[1] = x + (1 - smallerfrac) * side / HEX_TRAFO;
  xvals[2] = x;
  xvals[3] = x - side / 2;
  xvals[4] = x;
  xvals[5] = xvals[0];
  yvals[0] = y + smallerfrac * side / HEX_TRAFO;
  yvals[1] = y - smallerfrac * side / HEX_TRAFO;
  yvals[2] = y - side / HEX_TRAFO;
  yvals[3] = y;
  yvals[4] = y + side / HEX_TRAFO;
  yvals[5] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_hex_smallotherhalf(double x, double y, const std::string& trafo,
                                             int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  double smallerfrac = 0.5;  // remove even more of the cell
  const int npar = 6;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x - smallerfrac * side / 2;
  xvals[1] = x - smallerfrac * side / 2;
  xvals[2] = x - side / 2;
  xvals[3] = x - side;
  xvals[4] = x - side / 2;
  xvals[5] = xvals[0];
  yvals[0] = y + side / HEX_TRAFO;
  yvals[1] = y - side / HEX_TRAFO;
  yvals[2] = y - side / HEX_TRAFO;
  yvals[3] = y;
  yvals[4] = y + side / HEX_TRAFO;
  yvals[5] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle);
  }
  return pline;
}
TPolyLine* geo_plot::draw_hex_jumper(double x, double y, const std::string& trafo,
                                     int colorArray[2 + MAX_INTERCELL_NUMBER], int style, int interCellStyle) {
  double side = get_par_val_double(trafo, "SIZE");
  side /= 2;
  double jumperwidth = side / 8;
  double jumperheight = side * 4;
  const int npar = 10;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x - jumperwidth * side;
  xvals[1] = x - jumperwidth * side;
  xvals[2] = x + jumperwidth * side;
  xvals[3] = x + jumperwidth * side;
  xvals[4] = x + side / HEX_TRAFO;
  xvals[5] = x + side / HEX_TRAFO;
  xvals[6] = x;
  xvals[7] = x - side / HEX_TRAFO;
  xvals[8] = x - side / HEX_TRAFO;
  xvals[9] = xvals[0];
  yvals[0] = y + (1 - jumperwidth) * side;
  yvals[1] = y + (1 - jumperwidth) * side + jumperheight * side;
  yvals[2] = y + (1 - jumperwidth) * side + jumperheight * side;
  yvals[3] = y + (1 - jumperwidth) * side;
  yvals[4] = y + side / 2;
  yvals[5] = y - side / 2;
  yvals[6] = y - side;
  yvals[7] = y - side / 2;
  yvals[8] = y + side / 2;
  yvals[9] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  int starthere = 3;
  if (drawInterPadValues) {
    draw_intercells(pline, npar, side, colorArray, interCellStyle, starthere);
  }
  return pline;
}
TPolyLine* geo_plot::draw_guardring(double x, double y, const std::string& trafo,
                                    int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  double side = get_par_val_double(trafo, "SIZE");
  double width = get_par_val_double(trafo, "WIDTH");
  if (width == 0.0) {
    width = 0.03;
  }
  double edgeShift = get_par_val_double(trafo, "EDGE_SHIFT");
  if (edgeShift == 0.0) {
    edgeShift = 0.54;
  }
  const int npar = 26;
  double xvals[npar];
  double yvals[npar];
  double angleoffset = edgeShift * 1. / 24 * 2 * TMath::Pi();  // smaller -> larger mouse bite distance
  for (int i = 0; i < npar / 2; ++i) {
    // double angle=i*2*TMath::Pi()/(npar/2-1)+TMath::Pi()/4;
    bool isEven = (i % 2 == 0);
    double angle = i * 2 * TMath::Pi() / (npar / 2 - 1) + TMath::Pi() / 4 + (isEven ? +angleoffset : -angleoffset);
    xvals[i] = x + side / 2 * TMath::Sin(angle);
    yvals[i] = y + side / 2 * TMath::Cos(angle);
  }
  for (int i = npar / 2; i < npar; ++i) {
    // double angle=i*2*TMath::Pi()/(npar/2-1)+TMath::Pi()/4;
    bool isEven = ((i - 1) % 2 == 0);
    double angle =
        (i - 1) * 2 * TMath::Pi() / (npar / 2 - 1) + TMath::Pi() / 4 + (isEven ? +angleoffset : -angleoffset);
    xvals[i] = x + side / 2 * (1 - width) * TMath::Sin(angle);
    yvals[i] = y + side / 2 * (1 - width) * TMath::Cos(angle);
  }
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_arrow(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                int style) {
  double side = get_par_val_double(trafo, "SIZE");
  const int npar = 8;
  double headSize = 0.4;
  double headHeight = 0.2;
  double xvals[npar];
  double yvals[npar];
  xvals[0] = x + side / 2;
  xvals[1] = x + side / 2 - side * headSize;
  xvals[2] = x + side / 2 - side * headSize;
  xvals[3] = x - side / 2;
  xvals[4] = x - side / 2;
  xvals[5] = x + side / 2 - side * headSize;
  xvals[6] = x + side / 2 - side * headSize;
  xvals[7] = xvals[0];
  yvals[0] = y;
  yvals[1] = y - 2 * side * headHeight;
  yvals[2] = y - side * headHeight;
  yvals[3] = y - side * headHeight;
  yvals[4] = y + side * headHeight;
  yvals[5] = y + side * headHeight;
  yvals[6] = y + 2 * side * headHeight;
  yvals[7] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_flash(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                int style) {
  double side = get_par_val_double(trafo, "SIZE");
  const int npar = 7;
  double xvals[npar];
  double yvals[npar];
  double unit = side / 4;
  xvals[0] = x + unit;
  xvals[1] = x;
  xvals[2] = x + unit;
  xvals[3] = x - unit;
  xvals[4] = x;
  xvals[5] = x - unit;
  xvals[6] = xvals[0];
  yvals[0] = y + 4 * unit;
  yvals[1] = y + unit;
  yvals[2] = y - unit;
  yvals[3] = y - 4 * unit;
  yvals[4] = y - unit;
  yvals[5] = y + unit;
  yvals[6] = yvals[0];
  TPolyLine* pline = create_poly(npar, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_person(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                 int style) {
  double side = get_par_val_double(trafo, "SIZE");
  const int npar = 100;
  double xvals[npar + 3];
  double yvals[npar + 3];
  // head
  double headSize = side * 0.6;
  for (int i = 0; i < npar; ++i) {
    double angle = -0.875 * TMath::Pi() + i * 1.75 * TMath::Pi() / npar;
    xvals[i] = x + headSize / 2 * TMath::Sin(angle);
    yvals[i] = y + side / 2 + headSize / 2 * TMath::Cos(angle);
  }
  // body
  xvals[npar] = x + side / 2;
  xvals[npar + 1] = x - side / 2;
  xvals[npar + 2] = xvals[0];
  yvals[npar] = y - side / 2;
  yvals[npar + 1] = y - side / 2;
  yvals[npar + 2] = yvals[0];
  TPolyLine* pline = create_poly(npar + 3, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_bar_polygram(double x, double y, const std::string& trafo, int bars,
                                       int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  double side = get_par_val_double(trafo, "SIZE");
  double width = get_par_val_double(trafo, "WIDTH");
  if (width == 0.0) {
    width = 0.5;
  }
  int npar = 3 * bars;
  double xvals[MAX_CELL_CORNER_NUMBER];
  double yvals[MAX_CELL_CORNER_NUMBER];
  double unitAngle = 2 * TMath::Pi() / npar;
  for (int i = 0; i < npar; ++i) {
    double angle = 3. * unitAngle / 2 + i * unitAngle;
    double angleModifier = 0;
    double thisSide = side;
    if (i % 3 == 0) {
      angleModifier = 0;
      thisSide = (1.1 * width - 0.04 * pow(width, 2) - 0.05 * pow(width, 3)) * side;
    }
    if (i % 3 == 1) {
      angleModifier = unitAngle / 2 - 3 * unitAngle / 2 * (width);
    }
    if (i % 3 == 2) {
      angleModifier = -unitAngle / 2 + 3 * unitAngle / 2 * (width);
    }
    angle += angleModifier;
    xvals[i] = x + thisSide / 2 * TMath::Sin(angle);
    yvals[i] = y + thisSide / 2 * TMath::Cos(angle);
  }
  xvals[npar] = xvals[0];
  yvals[npar] = yvals[0];
  TPolyLine* pline = create_poly(npar + 1, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_bar_trigram(double x, double y, const std::string& trafo,
                                      int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_bar_polygram(x, y, trafo, 3, colorArray, style);
}
TPolyLine* geo_plot::draw_plus(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                               int style) {
  return draw_bar_polygram(x, y, trafo, 4, colorArray, style);
}
TPolyLine* geo_plot::draw_bar_pentagram(double x, double y, const std::string& trafo,
                                        int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_bar_polygram(x, y, trafo, 5, colorArray, style);
}
TPolyLine* geo_plot::draw_bar_hexagram(double x, double y, const std::string& trafo,
                                       int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_bar_polygram(x, y, trafo, 6, colorArray, style);
}
TPolyLine* geo_plot::draw_bar_heptagram(double x, double y, const std::string& trafo,
                                        int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_bar_polygram(x, y, trafo, 7, colorArray, style);
}
TPolyLine* geo_plot::draw_bar_octagram(double x, double y, const std::string& trafo,
                                       int colorArray[2 + MAX_INTERCELL_NUMBER], int style) {
  return draw_bar_polygram(x, y, trafo, 8, colorArray, style);
}
TPolyLine* geo_plot::draw_heart(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                int style) {
  double side = get_par_val_double(trafo, "SIZE");
  const int npar = 100;
  double xvals[npar + 3];
  double yvals[npar + 3];
  double bumpSize = side / 4;
  // bumps
  for (int i = 0; i < npar; ++i) {
    double angle = i * 2 * TMath::Pi() / npar;
    if (i < npar / 2) {
      angle += TMath::Pi() * 5 / 4;
      xvals[i] = x - bumpSize + sqrt(2) * bumpSize * TMath::Sin(angle);
      yvals[i] = y + bumpSize + sqrt(2) * bumpSize * TMath::Cos(angle);
    } else {
      angle += TMath::Pi() * 3 / 4;
      xvals[i] = x + bumpSize + sqrt(2) * bumpSize * TMath::Sin(angle);
      yvals[i] = y + bumpSize + sqrt(2) * bumpSize * TMath::Cos(angle);
    }
  }
  // body
  xvals[npar] = x;
  xvals[npar + 1] = xvals[0];
  yvals[npar] = y - side / 2;
  yvals[npar + 1] = yvals[0];
  TPolyLine* pline = create_poly(npar + 2, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_flower(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                                 int style) {
  double side = get_par_val_double(trafo, "SIZE");
  const int npar = 100;
  double xvals[npar + 3];
  double yvals[npar + 3];
  double bumpSize = side * 0.25;
  // bumps
  for (int i = 0; i < npar; ++i) {
    double angle = i * 4 * TMath::Pi() / npar;
    if (i < npar * 1 / 4) {
      angle += TMath::Pi() * 5 / 4;
      xvals[i] = x - bumpSize + sqrt(2) * bumpSize * TMath::Sin(angle);
      yvals[i] = y + bumpSize + sqrt(2) * bumpSize * TMath::Cos(angle);
    } else if (i < npar * 2 / 4) {
      angle += TMath::Pi() * 3 / 4;
      xvals[i] = x + bumpSize + sqrt(2) * bumpSize * TMath::Sin(angle);
      yvals[i] = y + bumpSize + sqrt(2) * bumpSize * TMath::Cos(angle);
    } else if (i < npar * 3 / 4) {
      angle += TMath::Pi() * 1 / 4;
      xvals[i] = x + bumpSize + sqrt(2) * bumpSize * TMath::Sin(angle);
      yvals[i] = y - bumpSize + sqrt(2) * bumpSize * TMath::Cos(angle);
    } else {
      angle += TMath::Pi() * 7 / 4;
      xvals[i] = x - bumpSize + sqrt(2) * bumpSize * TMath::Sin(angle);
      yvals[i] = y - bumpSize + sqrt(2) * bumpSize * TMath::Cos(angle);
    }
  }
  // body
  xvals[npar] = xvals[0];
  yvals[npar] = yvals[0];
  TPolyLine* pline = create_poly(npar + 1, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
TPolyLine* geo_plot::draw_moon(double x, double y, const std::string& trafo, int colorArray[2 + MAX_INTERCELL_NUMBER],
                               int style) {
  double side = get_par_val_double(trafo, "SIZE");
  const int npar = 100;
  double xvals[npar + 3];
  double yvals[npar + 3];
  // bumps
  for (int i = 0; i < npar; ++i) {
    if (i < npar / 2) {
      double angle = i * 2 * TMath::Pi() / npar;
      double radius = side / 2;
      xvals[i] = x - side / 4 + radius * TMath::Sin(angle);
      yvals[i] = y + radius * TMath::Cos(angle);
    } else {
      double angle = TMath::Pi() + (npar / 2 - i) * 2 * TMath::Pi() / npar;
      double radius = side - TMath::Sin(angle) * side / 2 * 0.7;
      xvals[i] = x - side / 4 + radius / 2 * TMath::Sin(angle);
      yvals[i] = y + radius / 2 * TMath::Cos(angle);
    }
  }
  // body
  xvals[npar] = xvals[0];
  yvals[npar] = yvals[0];
  TPolyLine* pline = create_poly(npar + 1, x, y, xvals, yvals, trafo, colorArray, style);
  pline->Draw("f");
  pline->Draw("");
  return pline;
}
void geo_plot::draw_polygons() {
  // optional pad scaling
  for (unsigned int i = 0; i < padNumbers.size(); ++i) {
    int num = padNumbers.at(i);
    int type = padTypes.at(i);

    if (padTypeScale[type] != 1) {
      double oldval = h_vals->GetBinContent(get_bin_number(num));
      double newval = oldval * padTypeScale[type];
      h_vals->SetBinContent(get_bin_number(num), newval);
      INFO_MSG("This is the %s value: %.3e. For display scaling it by %.2f to %.3e!", get_pad_type_string(num).c_str(),
               oldval, padTypeScale[type], h_vals->GetBinContent(get_bin_number(num)));
    }
    if (padScale[num] != 1) {
      double oldval = h_vals->GetBinContent(get_bin_number(num));
      double newval = oldval * padScale[num];
      h_vals->SetBinContent(get_bin_number(num), newval);
      INFO_MSG("This is the pad %d value: %.3e. For display scaling it by %.2f to %.3e!", num, oldval, padScale[num],
               h_vals->GetBinContent(get_bin_number(num)));
    }
  }

  // make sure all requested color values are within range
  double valMin = h_vals->GetMinimum();
  double valMax = h_vals->GetMaximum();
  double valRange = valMax - valMin;
  h_vals->SetMinimum(valMin - valRange * 0.01);
  h_vals->SetMaximum(valMax + valRange * 0.01);

  // first get colors of the polygons
  gPad->Update();
  auto* palette = dynamic_cast<TPaletteAxis*>(h_vals->GetListOfFunctions()->FindObject("palette"));
  int currentpad(0);
  bool skipMessage = true;
  for (int i = 0; i < nPads + 1; ++i) {
    if (nPads == 0) {
      for (int i = 0; i < MAX_PAD_NUMBER; ++i) {
        padTypeScale[i] = 1;
        padScale[i] = 1;
      }
      break;
    }
    int this_bin = (zeroBased ? i + 1 : i);
    if (!is_in_vector(i, padNumbers)) {
      if (skipMessage) {
        DEBUG_MSG(
            "Bin %d (%d) content %f, color_id %d not in geo file -> skip it. The same is applied to following pads "
            "not in geo file.",
            this_bin, get_bin_number(this_bin), h_vals->GetBinContent(get_bin_number(this_bin)),
            palette->GetValueColor(h_vals->GetBinContent(get_bin_number(this_bin))));
      }
      skipMessage = false;
      continue;
    }
    skipMessage = true;

    // first for main cell value histogram
    double binc = h_vals->GetBinContent(get_bin_number(this_bin));
    Int_t color_id;
    color_id = palette->GetValueColor(binc);
    if (useLogVal) {
      color_id = palette->GetValueColor(TMath::Log10(binc));
    }
    if (binc == 0) {
      DEBUG_MSG("retrieving color bin %d/%d (global %d): %d but bin content %f -> set white", this_bin, nPads,
                get_bin_number(this_bin), color_id, binc);
      h_colors->SetBinContent(currentpad + 1, 0);
    } else if (zMin != zMax && binc > zMax) {
      DEBUG_MSG("retrieving color bin %d/%d (global %d): %d but bin content %f > max %f -> set red", this_bin, nPads,
                get_bin_number(this_bin), color_id, binc, zMax);
      h_colors->SetBinContent(currentpad + 1, 2);
    } else if (binc > MAX_DOUBLE) {
      DEBUG_MSG("retrieving color bin %d/%d (global %d): %d but bin content %e > max double %e -> set red", this_bin,
                nPads, get_bin_number(this_bin), color_id, binc, MAX_DOUBLE);
      h_colors->SetBinContent(currentpad + 1, 2);
    } else if (zMin != zMax && binc < zMin) {
      DEBUG_MSG("retrieving color bin %d/%d (global %d): %d but bin content %f < min %f -> set grey", this_bin, nPads,
                get_bin_number(this_bin), color_id, binc, zMin);
      h_colors->SetBinContent(currentpad + 1, 18);
    } else if (binc != binc) {
      DEBUG_MSG("retrieving color bin %d/%d (global %d): %d but bin content %f -> set grey", this_bin, nPads,
                get_bin_number(this_bin), color_id, binc);
      h_colors->SetBinContent(currentpad + 1, 18);
    } else {
      DEBUG_MSG("retrieving color bin %d/%d (global %d) with content %f: %d", this_bin, nPads, get_bin_number(this_bin),
                binc, color_id);
      h_colors->SetBinContent(currentpad + 1, color_id);
    }

    // then for inter-cell histograms
    for (int j = 0; j < nInterCells; ++j) {
      double inter_zMax = h_vals->GetMaximum();
      double inter_zMin = h_vals->GetMinimum();
      binc = h_vals_inter[j]->GetBinContent(get_bin_number(this_bin));
      color_id = palette->GetValueColor(binc);
      if (binc == 0) {
        if (drawInterPadValues) {
          DEBUG_MSG(" intercell %d color: %d but bin content %f -> set white", j, color_id, binc);
        }
        h_colors_inter[j]->SetBinContent(currentpad + 1, 0);
      } else if (inter_zMin != inter_zMax && binc > inter_zMax) {
        if (drawInterPadValues) {
          DEBUG_MSG(" intercell %d color: %d but bin content %f > max of main hexagons %f -> set red", j, color_id,
                    binc, inter_zMax);
        }
        h_colors_inter[j]->SetBinContent(currentpad + 1, 2);
      } else if (inter_zMin != inter_zMax && binc < inter_zMin) {
        if (drawInterPadValues) {
          DEBUG_MSG(" intercell %d color: %d but bin content %f < min of main hexagons %f -> set grey", j, color_id,
                    binc, inter_zMin);
        }
        h_colors_inter[j]->SetBinContent(currentpad + 1, 18);
      } else if (binc != binc) {
        if (drawInterPadValues) {
          DEBUG_MSG(" intercell %d color: %d but bin content %f -> set grey", j, color_id, binc);
        }
        h_colors_inter[j]->SetBinContent(currentpad + 1, 18);
      } else {
        if (drawInterPadValues) {
          DEBUG_MSG(" intercell %d color with content %f: %d", j, binc, color_id);
        }
        h_colors_inter[j]->SetBinContent(currentpad + 1, color_id);
      }
    }
    ++currentpad;
  }

  if (overwritePadColors != -1) {
    for (int j = 0; j < h_colors->GetNbinsX(); j++) {
      h_colors->SetBinContent(j + 1, overwritePadColors);
    }
  }

  // now draw the polygons
  int colourBin = 0;
  std::vector<double> numberXPos;
  std::vector<double> numberYPos;
  std::vector<int> numberPadCol;
  for (unsigned int i = 0; i < padNumbers.size(); ++i) {
    int num = padNumbers.at(i);
    double xPos = padXPositions.at(i);
    double yPos = padYPositions.at(i);
    int type = padTypes.at(i);
    std::string padTrafo = padTrafos.at(i);
    double size = get_par_val_double(padTrafo, "SIZE");

    DEBUG_MSG("Drawing hex pad %d xPos %f yPos %f type %d", num, xPos, yPos, type);

    // int style=is_in_vector(i+1,highlightPads)?3003:1001;
    int style = 1001;
    int interCellStyle = 1001;
    // get color array
    int colorArray[2 + MAX_INTERCELL_NUMBER];
    for (int& j : colorArray) {
      j = 0;
    }
    if (num >= 0) {
      ++colourBin;
      colorArray[0] = int(h_colors->GetBinContent(colourBin));  // fill
      colorArray[1] = int(h_colors->GetBinContent(colourBin));  // line
      for (int j = 0; j < nInterCells; ++j) {
        colorArray[2 + j] = int(h_colors_inter[j]->GetBinContent(colourBin));
      }
    } else {
      colorArray[0] = 0;
      colorArray[1] = 0;
      for (int j = 0; j < nInterCells; ++j) {
        colorArray[2 + j] = 0;
      }
    }

    if (is_special_pad(num)) {
      if (contains(treatOtherPads, "greyout")) {
        colorArray[0] = 18;
        colorArray[1] = 18;
      }
      if (contains(treatOtherPads, "dense")) {
        style = 3001;
      } else if (contains(treatOtherPads, "dotted")) {
        style = 3002;
      } else if (contains(treatOtherPads, "checked")) {
        style = 3144;
      } else if (contains(treatOtherPads, "loose")) {
        style = 3003;
      }
      if (contains(treatOtherPads, "blackborder")) {
        colorArray[1] = kBlack;
      }
    } else if (!highlightPads.empty()) {
      if (contains(treatOtherPads, "redborder")) {
        colorArray[1] = kRed;
      }
    }

    // draw polygons
    TPolyLine* pline = nullptr;
    // clang-format off
    if        (type == 1) {  pline = draw_guardring(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 2) {  pline = draw_hex_half(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 3) {  pline = draw_hex_mousebite(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 4) {  pline = draw_hex_ring(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 7) {  pline = draw_pentagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 8) {  pline = draw_hexagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 9) {  pline = draw_heptagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 10) { pline = draw_hex_jumper(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 11) { pline = draw_hex_cornercut(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 12) { pline = draw_square(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 13) { pline = draw_triangle(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 14) { pline = draw_circle(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 15) { pline = draw_plus(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 16) { pline = draw_heart(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 17) { pline = draw_pent(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 18) { pline = draw_hept(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 19) { pline = draw_oct(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 20) { pline = draw_tetragram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 21) { pline = draw_hex_otherhalf(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 22) { pline = draw_hex_smallotherhalf(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 23) { pline = draw_diamond(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 24) { pline = draw_octagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 25) { pline = draw_arrow(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 26) { pline = draw_flash(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 27) { pline = draw_person(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 28) { pline = draw_triangle_ring(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 29) { pline = draw_square_ring(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 30) { pline = draw_pent_ring(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 31) { pline = draw_hept_ring(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 32) { pline = draw_oct_ring(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 33) { pline = draw_circle_ring(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 34) { pline = draw_flower(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 35) { pline = draw_bar_trigram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 36) { pline = draw_bar_pentagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 37) { pline = draw_bar_hexagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 38) { pline = draw_bar_heptagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 39) { pline = draw_bar_octagram(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 40) { pline = draw_moon(xPos, yPos, padTrafo, colorArray, style);
    } else if (type == 41) { pline = draw_hex_cornercut_long(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else if (type == 42) { pline = draw_hex_cornercut_long_cut(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    } else {                 pline = draw_hex(xPos, yPos, padTrafo, colorArray, style, interCellStyle);
    }
    // clang-format on

    // draw numbers
    double xRawOffset = 0;
    double yRawOffset = 0;
    if (type == 1) {
      xRawOffset = -0.535 * size / HEX_TRAFO;
    } else if (type == 4 || type == 28 || type == 29 || type == 30 || type == 31 || type == 32 || type == 33) {
      yRawOffset = 0.4 * size;
    } else if (type == 3) {
      xRawOffset = 0.1 * size;
    } else if (type == 10) {
      yRawOffset = -0.3 * size;
    } else if (type == 16) {
      yRawOffset = -0.3 * size;
    } else if (type == 21 || type == 22) {
      xRawOffset = 0.1 * size;
    } else if (type == 16) {
      xRawOffset = -0.1 * size;
    }
    double totalRotation = get_total_sum_par_val_double(padTrafo, "ROTATE");
    if (!get_par_val(padTrafo, "INVERT").empty()) {
      xRawOffset *= -1;
    }
    totalRotation *= 2. * TMath::Pi() / 360;
    double thisCos = TMath::Cos(totalRotation);
    double thisSin = TMath::Sin(totalRotation);
    double xOffset = xRawOffset * thisCos - yRawOffset * thisSin;
    double yOffset = yRawOffset * thisCos + xRawOffset * thisSin;
    double xOffsetFromGeoFile = get_par_val_double(padTrafo, "TEXT_X_OFFSET");
    double yOffsetFromGeoFile = get_par_val_double(padTrafo, "TEXT_Y_OFFSET");
    if (xOffsetFromGeoFile != 0.0) {
      xOffset = xOffsetFromGeoFile;
    }
    if (yOffsetFromGeoFile != 0.0) {
      yOffset = yOffsetFromGeoFile;
    }
    double* center = get_poly_center_of_gravity(pline, true);
    double xCenter = center[0];
    double yCenter = center[1];
    numberXPos.push_back(xCenter + xOffset);
    numberYPos.push_back(yCenter + yOffset);
    numberPadCol.push_back(colorArray[0]);
  }
  for (unsigned int i = 0; i < padNumbers.size(); ++i) {
    draw_number(numberXPos.at(i), numberYPos.at(i), numberPadCol.at(i), i);
  }
  if (useLogVal) {
    gPad->SetLogz();
  }

  h_vals->GetXaxis()->SetAxisColor(0, 0);
  h_vals->GetYaxis()->SetAxisColor(0, 0);
  h_vals->GetZaxis()->SetAxisColor(0, 0);
  if (!noAxis) {
    h_vals->Draw("axiscolzsame");  // remove any overlap with zaxis
  }
}
bool geo_plot::is_special_pad(int padNumber) {
  return (!highlightPads.empty() &&
          find(highlightPads.begin(), highlightPads.end(), padNumber) - highlightPads.begin() >=
              int(highlightPads.size()));
}
void geo_plot::draw_number(double xPos, double yPos, int color, int i) {
  int num = padNumbers.at(i);
  std::string padText = get_par_val(padTrafos.at(i), "TEXT");
  double padcont = h_vals->GetBinContent(get_bin_number(zeroBased ? num + 1 : num));
  auto* padNum = new TPaveText(xPos, yPos, xPos, yPos);
  padNum->SetTextAlign(22);
  padNum->SetFillColor(color);
  padNum->SetLineColor(color);
  padNum->SetTextSize(0.02);
  padNum->SetTextColor(textColor);
  if (!padText.empty()) {
    padNum->AddText(Form("%s", padText.c_str()));
  } else if (padNumOpt == 0) {
    padNum->AddText(Form("%d", num));
  } else if (padNumOpt == 1) {
    if (padcont != padcont) {
      padNum->AddText("NaN");
    } else if (padcont == 0) {
      padNum->AddText("");
    } else if (padcont > MAX_DOUBLE) {
      padNum->AddText("OF");
    } else {
      if (padcont > 999) {
        padNum->AddText(Form("%.*e", valPrecision, padcont));
      } else {
        padNum->AddText(Form("%.*f", valPrecision, padcont));
      }
    }
  } else if (padNumOpt == 2) {
    padNum->AddText(Form("%d", get_pad_type(num)));
  } else if (padNumOpt == 3) {
    return;
  }

  if (is_special_pad(num)) {
    if (contains(treatOtherPads, "nonumbers")) {
      return;
    }
    if (contains(treatOtherPads, "smallnumbers")) {
      padNum->SetTextSize(0.013);
    }
  }
  padNum->Draw("same");
}
void geo_plot::draw_info() {
  bool draw_all = infoOption == "ALL";
  bool draw_none = (infoOption.find("NONE") < infoOption.size());
  bool draw_topleft = draw_all || (infoOption.find("TOPLEFT") < infoOption.size());
  bool draw_topright = draw_all || (infoOption.find("TOPRIGHT") < infoOption.size());
  bool draw_lowleft = draw_all || (infoOption.find("LOWLEFT") < infoOption.size());
  bool draw_lowright = draw_all || (infoOption.find("LOWRIGHT") < infoOption.size());
  if (draw_none) {
    return;
  }

  TPaveText* padNum = nullptr;

  if (draw_topleft) {
    padNum = new TPaveText(-6, 6.5, 3, 7.5);
    padNum->SetFillColor(kNone);
    padNum->SetLineColor(kNone);
    //    if (draw_topleft && draw_topright) {
    //      padNum->SetTextSize(0.035);
    //    } else {
    //      padNum->SetTextSize(0.05);
    //    }
    padNum->SetTextAlign(12);
    padNum->SetTextColor(kBlack);
    padNum->AddText(topLeftInfo.c_str());
    padNum->Draw("same");
  }

  if (draw_topright && topRightInfo.empty()) {
    int nlines(1);
    std::vector<int> types_treated;
    for (int type : padTypes) {
      if (std::find(types_treated.begin(), types_treated.end(), type) != types_treated.end()) {
        continue;
      }
      if (padTypeScale[type] != 1) {
        ++nlines;
      }
      types_treated.push_back(type);
    }
    for (int i = 0; i <= nPads; ++i) {
      if (padScale[i] != 1) {
        ++nlines;
      }
    }
    padNum = new TPaveText(6, 7 - 0.3 * nlines, 6, 7);
    padNum->SetFillColor(kNone);
    padNum->SetLineColor(kNone);
    padNum->SetTextSize(0.05);
    padNum->SetTextAlign(32);
    padNum->SetTextColor(kBlack);
    if (nlines > 1) {
      padNum->AddText("Scales applied to pad value:");
    }
    types_treated.clear();
    for (int type : padTypes) {
      if (std::find(types_treated.begin(), types_treated.end(), type) != types_treated.end()) {
        continue;
      }
      if (padTypeScale[type] != 1) {
        padNum->AddText(Form("%s: %.0e", get_type_string(type).c_str(), padTypeScale[type]));
      }
      types_treated.push_back(type);
    }
    for (int i = 0; i <= nPads; ++i) {
      if (padScale[i] != 1) {
        padNum->AddText(Form("Pad %d (%s): %.0e", i, get_pad_type_string(i).c_str(), padScale[i]));
      }
    }
    padNum->Draw("same");
  } else if (draw_topright) {
    padNum = new TPaveText(6, 6.8, 6, 6.8);
    padNum->SetFillColor(kNone);
    padNum->SetLineColor(kNone);
    if (draw_topleft && draw_topright) {
      padNum->SetTextSize(0.035);
    } else {
      padNum->SetTextSize(0.05);
    }
    if (topRightInfo.find("verage ") != std::string::npos) {
      padNum->SetTextSize(0.035);
    }
    padNum->SetTextAlign(32);
    padNum->SetTextColor(kBlack);
    padNum->AddText(topRightInfo.c_str());
    padNum->Draw("same");
  }

  if (draw_lowleft) {
    padNum = new TPaveText(-6, -6.5, -6, -6.5);
    padNum->SetFillColor(kNone);
    padNum->SetLineColor(kNone);
    if (draw_lowleft && draw_lowright) {
      padNum->SetTextSize(0.035);
    } else {
      padNum->SetTextSize(0.05);
    }
    padNum->SetTextAlign(12);
    padNum->SetTextColor(kBlack);
    padNum->AddText(lowLeftInfo.c_str());
    padNum->Draw("same");
  }

  if (draw_lowright) {
    padNum = new TPaveText(6, -7.0, 6, -7.0);
    padNum->SetFillColor(kNone);
    padNum->SetLineColor(kNone);
    if (draw_lowleft && draw_lowright) {
      padNum->SetTextSize(0.035);
    } else {
      padNum->SetTextSize(0.05);
    }
    padNum->SetTextAlign(32);
    padNum->SetTextColor(kBlack);
    padNum->AddText(lowRightInfo.c_str());
    padNum->Draw("same");
  }
}
void geo_plot::draw_logo() {
  TImage* logo = TImage::Open("doc/logo.png");
  if (logo == nullptr) {
    printf("Could not create an image... exit\n");
    return;
  }

  float d = 0.05;
  float x = 0.01;
  float y = 0.99;
  auto* pLogo = new TPad("logo", "logo", x, y, x + d * logo->GetWidth() / logo->GetHeight(),
                         y - d * logo->GetWidth() / logo->GetHeight());

  pLogo->Draw("same");
  pLogo->cd();
  logo->Draw("same");
}
void geo_plot::draw() {
  fill_geo_info();
  gStyle->SetOptStat(0);

  h_vals->GetZaxis()->SetTitle(zTitle.c_str());
  h_vals->GetZaxis()->SetTitleSize(0.06);
  h_vals->GetZaxis()->SetLabelSize(0.05);
  h_vals->SetTitle("");
  adjust_z_range();

  if (zMin != 0 || zMax != 0) {
    h_vals->GetZaxis()->SetRangeUser(zMin, zMax);
  }
  double rightMargin = 0.17;
  if (h_vals->GetMaximum() >= 10000) {
    rightMargin = 0.20;
    h_vals->GetZaxis()->SetTitleOffset(1.3);
  } else if (h_vals->GetMaximum() >= 1000) {
    rightMargin = 0.20;
    h_vals->GetZaxis()->SetTitleOffset(1.2);
  } else if (h_vals->GetMaximum() >= 100) {
    rightMargin = 0.20;
    h_vals->GetZaxis()->SetTitleOffset(1.1);
  } else {
    h_vals->GetZaxis()->SetTitleOffset(0.9);
  }

  double canvWidth = 750;
  auto* c_hex = new TCanvas("c_hex", "c_hex", 800, 500, int(canvWidth), int(noAxis ? canvWidth : canvWidth / 1.1));
  c_hex->cd();
  if (noAxis) {
    gPad->SetTopMargin(0);
    gPad->SetRightMargin(0.005);
    gPad->SetBottomMargin(0);
    gPad->SetLeftMargin(0);
  } else {
    if (useLogVal) {
      gPad->SetLogz();
    }
    gPad->SetTopMargin(0.06);
    gPad->SetRightMargin(float(rightMargin));
    gPad->SetBottomMargin(0.02);
    gPad->SetLeftMargin(0);
  }
  gStyle->SetPalette(paletteNumber);
  gStyle->SetNumberContours(100);
  h_vals->Draw("colz");
  draw_white_box();
  draw_polygons();
  // draw_numbers();
  draw_info();
  draw_logo();
  c_hex->Print(outputFile.c_str());
  if (testInfo) {
    printf("geo_plot::draw %s\n", unique_ID(h_vals).c_str());
  }
  delete c_hex;
}
